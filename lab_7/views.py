from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os

from django.core import serializers

response = {}
csui_helper = CSUIhelper()

def index(request):
    page = request.GET.get('page', 1)
    mahasiswa_list, prev_url, next_url = csui_helper.instance.get_mahasiswa_list(page)

    friend_list = Friend.objects.all()
    html = 'lab_7/lab_7.html'

    response = {"mahasiswa_list": mahasiswa_list,
                "friend_list": friend_list,
                "prev_url": prev_url,
                "next_url": next_url,
                "page": page
                }
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request): # update
    if request.method == 'GET':
        friends = [obj.as_dict() for obj in Friend.objects.all()]
        return JsonResponse({"results": friends}, content_type='application/json')

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        return JsonResponse(friend.as_dict())

@csrf_exempt
def delete_friend(request):
    if request.method == 'POST':
        npm = request.POST['npm']
        Friend.objects.filter(npm=npm).delete()
        return HttpResponseRedirect('/lab-7/friend-list/')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)
