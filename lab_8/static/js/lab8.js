// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '1314419631996031',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
    FB.getLoginStatus(function(response) {
      console.log("status : "+ response['status']);
      render(response['status']==='connected');
    });
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
                        '<div class="container">' +
                            '<img class="cover" src="' + user.cover.source + '" alt="cover"/>' +
                            '<div class="data">' +
                              '<div class="row">' +
                                '<div class="col-sm-4">' +
                                '<img class="picture" id="gambarprofil" src="' + user.picture.data.url + '" alt="profpic" />' +
                                '</div>' +
                                '<div class="col-sm-8">' +
                                '<h1>' + user.name + '</h1>' +
                                '<h2>' + user.about + '</h2>' +
                                '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
                                '</div>' +
                              '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="container">' +
                            '<div class="row">' +
                                '<div class="col-xs-12">' +
                                    '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-xs-12 buttons">' +
                                    '<button class="postStatus" onclick="postStatus()">Post</button>' +
                                    '<button class="logout" onclick="facebookLogout()">Logout</button>' +
                                '</div>' +
                            '</div>' +

                        '</div>'
                    );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        $('#lab8').append("<div class='container' id='feeds'></div>");

        getUserFeed(feed => {
            feed.data.map(value => {
                // Render feed, kustomisasi sesuai kebutuhan.
                let id  = "'" + value.id + "'";
                if (value.message && value.story) {
                    $('#feeds').append(
                        '<div class="feed">' +
                        '<h3>' + value.message + '</h3>' +
                        '<h4>' + value.story + '</h4>' +
                        '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
                        '</div>'
                    );
                } else if (value.message) {
                    $('#feeds').append(
                        '<div class="feed">' +
                        '<h3>' + value.message + '</h3>' +
                        '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
                        '</div>'
                    );
                } else if (value.story) {
                    $('#feeds').append(
                        '<div class="feed">' +
                        '<h4>' + value.story + '</h4>' +
                        '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
                        '</div>'
                    );
                }
            });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html(
        '<div class="row">' +
          '<div class="col-sm-4">.col-sm-4</div>' +
          '<div class="col-sm-4">' +
              '<button class="loginBtn loginBtn--facebook" onclick="facebookLogin()">Login with Facebook</button>' +
          '</div>' +
          '<div class="col-sm-4">.col-sm-4</div>' +
        '</div>' 
        
        );
    }
  };

  const facebookLogin = () => {
      FB.login(response => {
          render(true);
      }, {scope:'public_profile,email,user_about_me,user_posts,publish_actions'});
  };

  const facebookLogout = () => {
    FB.logout(response => {
        render(false);
    });
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
    FB.api('/me?fields=cover,picture,name,about,email,gender', 'GET', function (response){
      fun(response);
    });
  };

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.api(
        "/me/feed",
        function (response) {
          if (response && !response.error) {
            fun(response);
          }
        }
    );
  };

  const postFeed = (message) => {
    FB.api(
        "/me/feed",
        "POST",
        {
            "message": message
        },
        function (response) {
          console.log(response);
          if (response && !response.error) {
            location.reload();
          }
        }
    );
  };

  const deleteFeed = (id) => {
    FB.api(
    id,
    "DELETE",
    function (response) {
      if (response && !response.error) {
        location.reload();
      }
    }
);
  }

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };
